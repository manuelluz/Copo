﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Copo
{
    public partial class Form1 : Form
    {

        private Copo meucopo;

        public Form1()
        {
            InitializeComponent();
            meucopo = new Copo();
            
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ComboBoxBebida.Items.Add("Imperial");
            ComboBoxBebida.Items.Add("Sangria");
            ComboBoxBebida.Items.Add("Vodka Cola");
            ComboBoxBebida.Items.Add("Caipirinha");
            ComboBoxQuantidade.Items.Add("0.30");
            ComboBoxQuantidade.Items.Add("0.50");
            ComboBoxQuantidade.Items.Add("0.75");
            LabelMessagem.Text = "";
            LabelStatus.Text = "0";

        }
        private void LabelStatus_Click(object sender, EventArgs e)
        {

        }

        private void ButtonMais_Click(object sender, EventArgs e)
        {

            LabelStatus.Text = ComboBoxQuantidade.Text;
            //LabelStatus.Text = meucopo.Contem.ToString();
            LabelMessagem.Text = "Copo cheio";
        }

        private void ComboBoxBebida_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ButtonMenos_Click(object sender, EventArgs e)
        {
            LabelStatus.Text = meucopo.Contem.ToString();
            LabelMessagem.Text = "Copo vazio";                     
        }

        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            //LabelMessagem.Text = meucopo.ToString();
            LabelMessagem.Text = "Copo Servido";
        }
    }
}
