﻿namespace Copo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.LabelNome = new System.Windows.Forms.Label();
            this.LabelBebida = new System.Windows.Forms.Label();
            this.LabelQuantidade = new System.Windows.Forms.Label();
            this.ComboBoxBebida = new System.Windows.Forms.ComboBox();
            this.ComboBoxQuantidade = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ButtonMenos = new System.Windows.Forms.Button();
            this.ButtonMais = new System.Windows.Forms.Button();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.LabelMessagem = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelNome
            // 
            this.LabelNome.AutoSize = true;
            this.LabelNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNome.Location = new System.Drawing.Point(14, 40);
            this.LabelNome.Name = "LabelNome";
            this.LabelNome.Size = new System.Drawing.Size(117, 31);
            this.LabelNome.TabIndex = 0;
            this.LabelNome.Text = "COPOS";
            // 
            // LabelBebida
            // 
            this.LabelBebida.AutoSize = true;
            this.LabelBebida.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBebida.Location = new System.Drawing.Point(16, 144);
            this.LabelBebida.Name = "LabelBebida";
            this.LabelBebida.Size = new System.Drawing.Size(65, 20);
            this.LabelBebida.TabIndex = 1;
            this.LabelBebida.Text = "Bebida";
            // 
            // LabelQuantidade
            // 
            this.LabelQuantidade.AutoSize = true;
            this.LabelQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelQuantidade.Location = new System.Drawing.Point(16, 172);
            this.LabelQuantidade.Name = "LabelQuantidade";
            this.LabelQuantidade.Size = new System.Drawing.Size(102, 20);
            this.LabelQuantidade.TabIndex = 2;
            this.LabelQuantidade.Text = "Quantidade";
            // 
            // ComboBoxBebida
            // 
            this.ComboBoxBebida.FormattingEnabled = true;
            this.ComboBoxBebida.Location = new System.Drawing.Point(167, 141);
            this.ComboBoxBebida.Name = "ComboBoxBebida";
            this.ComboBoxBebida.Size = new System.Drawing.Size(167, 21);
            this.ComboBoxBebida.TabIndex = 3;
            this.ComboBoxBebida.Text = "Selecionar bebida";
            this.ComboBoxBebida.SelectedIndexChanged += new System.EventHandler(this.ComboBoxBebida_SelectedIndexChanged);
            // 
            // ComboBoxQuantidade
            // 
            this.ComboBoxQuantidade.FormattingEnabled = true;
            this.ComboBoxQuantidade.Location = new System.Drawing.Point(167, 172);
            this.ComboBoxQuantidade.Name = "ComboBoxQuantidade";
            this.ComboBoxQuantidade.Size = new System.Drawing.Size(167, 21);
            this.ComboBoxQuantidade.TabIndex = 4;
            this.ComboBoxQuantidade.Text = "Selecionar quantidade";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ButtonMenos);
            this.groupBox1.Controls.Add(this.ButtonMais);
            this.groupBox1.Controls.Add(this.LabelStatus);
            this.groupBox1.Location = new System.Drawing.Point(20, 234);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Encher";
            // 
            // ButtonMenos
            // 
            this.ButtonMenos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonMenos.Location = new System.Drawing.Point(22, 43);
            this.ButtonMenos.Name = "ButtonMenos";
            this.ButtonMenos.Size = new System.Drawing.Size(75, 23);
            this.ButtonMenos.TabIndex = 2;
            this.ButtonMenos.Text = "-";
            this.ButtonMenos.UseVisualStyleBackColor = true;
            this.ButtonMenos.Click += new System.EventHandler(this.ButtonMenos_Click);
            // 
            // ButtonMais
            // 
            this.ButtonMais.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonMais.Location = new System.Drawing.Point(173, 43);
            this.ButtonMais.Name = "ButtonMais";
            this.ButtonMais.Size = new System.Drawing.Size(75, 23);
            this.ButtonMais.TabIndex = 1;
            this.ButtonMais.Text = "+";
            this.ButtonMais.UseVisualStyleBackColor = true;
            this.ButtonMais.Click += new System.EventHandler(this.ButtonMais_Click);
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(123, 50);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(22, 16);
            this.LabelStatus.TabIndex = 0;
            this.LabelStatus.Text = "- -";
            this.LabelStatus.Click += new System.EventHandler(this.LabelStatus_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonCriar.Location = new System.Drawing.Point(259, 364);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(75, 56);
            this.ButtonCriar.TabIndex = 6;
            this.ButtonCriar.Text = "Servir";
            this.ButtonCriar.UseVisualStyleBackColor = true;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // LabelMessagem
            // 
            this.LabelMessagem.AutoSize = true;
            this.LabelMessagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMessagem.Location = new System.Drawing.Point(41, 385);
            this.LabelMessagem.Name = "LabelMessagem";
            this.LabelMessagem.Size = new System.Drawing.Size(127, 16);
            this.LabelMessagem.TabIndex = 7;
            this.LabelMessagem.Text = "Label Messagem";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(145, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(189, 85);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 432);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LabelMessagem);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ComboBoxQuantidade);
            this.Controls.Add(this.ComboBoxBebida);
            this.Controls.Add(this.LabelQuantidade);
            this.Controls.Add(this.LabelBebida);
            this.Controls.Add(this.LabelNome);
            this.Name = "Form1";
            this.Text = "Copos";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelNome;
        private System.Windows.Forms.Label LabelBebida;
        private System.Windows.Forms.Label LabelQuantidade;
        private System.Windows.Forms.ComboBox ComboBoxBebida;
        private System.Windows.Forms.ComboBox ComboBoxQuantidade;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ButtonMenos;
        private System.Windows.Forms.Button ButtonMais;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Label LabelMessagem;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

